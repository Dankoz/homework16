#include <iostream>
#include <ctime>

int main()
{
    time_t now = time(0);
    tm ltm;
    localtime_s(&ltm, &now);

    int day = ltm.tm_mday;
    const int N = 5;
    int array[N][N];
    
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            std::cout << array[i][j];
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;

    int sum = 0;

    for (int i = 0; i < N; ++i)
    {
        sum += array[day % N][i];
    }
    std::cout << sum << std::endl;
}